<style>
body, html {
  margin: 0;
  padding: 0;
}
header {
  background-color: #92a8d1;
  margin-top: -20px;
  padding: 30px;
  text-align: center;
  font-size: 35px;
  color: white;
}

.content {
  display: flex;
  min-height: 100vh;
}

.sidebar {
  list-style-type: none;
  padding: 0;
  width: 0;
  overflow: hidden;
  white-space: nowrap;
  background-color: #00FF7F;
  margin: 0;
}
.sidebar a {
  text-align: right;
  padding: 8px 8px 8px 32px;
  text-decoration: none;
  font-size: 25px;
  color: #818181;
  display: block;
  transition: 0.3s;
}

button {
   position: absolute;
  top: 130px;
  left: -2.5%;
  font-size: 36px;
  margin-left: 50px;
}
* {
  box-sizing: border-box;
}
.column {
  float: left;
  width: 300px;
  border: 5px solid black;
  margin: 60px;
  padding: 10px;
  height: 300px;
  background-color:#aaa;
}
.row:after {
  content: "";
  display: table;
  clear: both;
}
.logout .btn {
  position: absolute;
  border: 1px solid white;
  background-color:red;
  color: #FFFFFF;
  text-decoration: none;
  top: 130px;
  right: 25px;
  font-size: 36px;
  margin-left: 50px;
}

</style>
<header>
  <h2>Welcome</h2>
</header>
<body>
<div class="content">
  <ul class="sidebar">
   <li><a href="#">About</a></li>
  </ul>
  
  <div class="row">
  <div class="column" >
    <h2>Column 1</h2>
    <p>text..</p>
  </div>
  <div class="column" >
    <h2>Column 2</h2>
    <p>text..</p>
  </div>
  <div class="column" >
    <h2>Column 3</h2>
    <p>text..</p>
  </div>
</div>
</div>
<div id="mySidenav" class="logout">
  <a href="javascript:void(0)" class="btn" >logout</a>
</div>

<button  type="button" class="button">&#9776</button>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script>
  $('.button').click(function() {
  var width = $('.sidebar').width();
  if(width == 0) {
     $('.sidebar').animate({width:"250"});
  } else {
     $('.sidebar').animate({width:"0"});
  }
});
</script>

